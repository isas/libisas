/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * This file is part of ISAS.
 *
 * Copyright (C) 2011 Thomas Bechtold <thomasbechtold@jpberlin.de>
 *
 * ISAS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * ISAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with ISAS.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <math.h>

#include "isas-tools.h"

/**
 * isas_tools_degrees_to_radians:
 * @deg: a value in degrees
 *
 * Convert the given value @deg (in degrees) to radians.
 *
 * Returns: the value in Radians
 */
gdouble 
isas_tools_degrees_to_radians (gdouble deg)
{
    return deg * M_PI / 180.0;
}

/**
 * isas_tools_radians_to_degrees:
 * @rad: a value in radians
 *
 * Convert the given value @rad (in radians) to degrees.
 *
 * Returns: the value in Degrees
 */
gdouble 
isas_tools_radians_to_degrees (gdouble rad)
{
    return rad * 180.0 / M_PI;
}


/**
 * isas_tools_solarconstant:
 * @dt: a #GDateTime object
 *
 * Calculates the solarconstant (E_0) (ca. 1367 W/(m*m) ) depending
 * on the earth -> sun distance for the given date/time.
 * See http://solardat.uoregon.edu/SolarRadiationBasics.html
 *
 * Returns: the solarconstant
 */
gdouble 
isas_tools_solarconstant (GDateTime* dt)
{
    gdouble sc = 0;
    gdouble b = 0;
    guint day_of_year = g_date_time_get_day_of_year (dt);

	if (g_date_is_leap_year (g_date_time_get_year (dt)))
		b = 2.0 * M_PI * day_of_year / 366.0;
	else
		b = 2.0 * M_PI * day_of_year / 365.0;
    
    //Calc factor sc
    sc = 1.00011 + (0.034221 * cos (b)) + (0.001280 * sin (b)) + (0.000719 * cos (2*b)) + (0.000077 * sin (2 * b));
    sc = sc * 1367.0;
    return sc;
}


/**
 * isas_tools_airmass:
 * @sun_height: sun height in degrees (0 < point_height <= 90 ; 90 degrees is zenith)
 *
 * Calculate the airmass (AM) depends on sun height
 *
 * Returns: the airmass(AM)
 */
gdouble 
isas_tools_airmass (gdouble sun_height)
{
    gdouble am = 0;
    if(sun_height > 0 && sun_height <= 90)
    {
		am = 1.0 / sin (isas_tools_degrees_to_radians (sun_height));
    } 
	else
    {
		g_warning ("%s: sun_height is : %f .out of range", __func__, sun_height);
    }
    return am;
}


/**
 * isas_tools_angle_sun_skypoint:
 * @sun_height: sun height in degrees (0 < point_height <= 90 ; 90 degrees is zenith)
 * @sun_azimuth: sun azimut in degrees (0 degree -> north, 90 degrees -> east, 180 degrees -> south, 270 degrees -> west)
 * @point_height: point altitude in degrees (0 < point_height <= 90 ; 90 degrees is zenith)
 * @point_azimuth: point azimut angle in degrees (0 degree -> north, 90 degrees -> east, 180 degrees -> south, 270 degrees -> west)
 *
 * Calculate the Angle between the Sun and a given Point on the Sky in Degrees.
 * See http://volker-quaschning.de/downloads/abschattungsverluste.pdf
 *
 * Returns: the angle in degrees between the sun and the point
 */
gdouble 
isas_tools_angle_sun_skypoint (gdouble sun_height, gdouble sun_azimuth, gdouble point_height, gdouble point_azimuth)
{
    return isas_tools_radians_to_degrees(acos (
					     sin (isas_tools_degrees_to_radians (sun_height)) *
					     sin (isas_tools_degrees_to_radians (point_height)) +
					     cos (isas_tools_degrees_to_radians (sun_height)) *
					     cos (isas_tools_degrees_to_radians (point_height)) *
					     cos (isas_tools_degrees_to_radians (ABS (sun_azimuth - point_azimuth)))));
}


/**
 * isas_tools_angle_sunnormal_plainnormal:
 * @sun_height: sun height in degrees (0 < point_height <= 90 ; 90 degrees is zenith)
 * @sun_azimuth: sun azimut in degrees (0 degree -> north, 90 degrees -> east, 180 degrees -> south, 270 degrees -> west)
 * @plain_height: angle of the plain in degrees. 0 degrees -> plain looks to the zenith ; 90 degrees -> plain looks to the horizont
 * @plain_azimuth: azimutangle of the plain in degrees. 0 degree -> Plain look to South ; 90 degree -> look to East ; 180 degree -> to North ; 270 degree -> 270 degree
 *
 * Calculate the Angle between the Normalvector of the Sun and the Normalvector of the Plain (in Dregees)
 * See: http://volker-quaschning.de/publis/buecher.html
 *
 * Returns: the angle in Degrees
 */
gdouble 
isas_tools_angle_sunnormal_plainnormal (gdouble sun_height, gdouble sun_azimuth, gdouble plain_height, gdouble plain_azimuth)
{
    return isas_tools_radians_to_degrees (acos (- cos (isas_tools_degrees_to_radians (sun_height)) *
					      sin (isas_tools_degrees_to_radians (plain_height)) *
					      cos (isas_tools_degrees_to_radians (sun_azimuth - plain_azimuth)) +
					      sin (isas_tools_degrees_to_radians (sun_height)) *
					      cos (isas_tools_degrees_to_radians (plain_height))));
}


/**
 * isas_tools_luminance_TO_irradiance:
 * @luminance: the value to convert to irradiance
 * 
 * Transform from luminance to Irradiance
 * see See http://volker-quaschning.de/downloads/abschattungsverluste.pdf
 * 
 * Returns: the irradiance
 */
gdouble 
isas_tools_luminance_TO_irradiance (gdouble luminance)
{
    gdouble lti;
    //lts = (1 / 125.4) * luminance;
    lti = luminance * (1.0 / 109.0);
    return lti;
}
