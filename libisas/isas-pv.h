/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * This file is part of ISAS.
 *
 * Copyright (C) 2011 Thomas Bechtold <thomasbechtold@jpberlin.de>
 *
 * ISAS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * ISAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with ISAS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ISAS_PV_H_
#define _ISAS_PV_H_


#include <math.h>
#include <glib.h>


/**
 * SECTION:isas-pv
 * @short_description: methods to model a photovoltaik module
 * @title: isas-pv
 * @section_id:
 * @stability: Unstable
 * @include: isas-pv.h
 *
 * The methods can be used to model a photovoltaik module
 */


G_BEGIN_DECLS


/**
 * IsasPvInstallationType:
 * @ISAS_PV_INSTALLATION_TYPE_FREE: +22 deg Celsius
 * @ISAS_PV_INSTALLATION_TYPE_ROOF_BIG_DISTANCE: +28 deg Celsius
 * @ISAS_PV_INSTALLATION_TYPE_ROOF_GOOD_AIRING: +29 deg Celsius
 * @ISAS_PV_INSTALLATION_TYPE_ROOF_BAD_AIRING: +32 deg Celsius
 * @ISAS_PV_INSTALLATION_TYPE_FACADE_GOOD_AIRING: +35 deg Celsius
 * @ISAS_PV_INSTALLATION_TYPE_FACADE_BAD_AIRING: +39 deg Celsius
 * @ISAS_PV_INSTALLATION_TYPE_ROOF_INTEGRATED_NO_AIRING: +43 deg Celsius
 * @ISAS_PV_INSTALLATION_TYPE_FACADE_INTEGRATED_NO_AIRING: +55 deg Celsius
 *
 * Enum values for module installation type.
 */
typedef enum {
    ISAS_PV_INSTALLATION_TYPE_FREE = 0,
    ISAS_PV_INSTALLATION_TYPE_ROOF_BIG_DISTANCE,
    ISAS_PV_INSTALLATION_TYPE_ROOF_GOOD_AIRING,
    ISAS_PV_INSTALLATION_TYPE_ROOF_BAD_AIRING,
    ISAS_PV_INSTALLATION_TYPE_FACADE_GOOD_AIRING,
    ISAS_PV_INSTALLATION_TYPE_FACADE_BAD_AIRING,
    ISAS_PV_INSTALLATION_TYPE_ROOF_INTEGRATED_NO_AIRING,
    ISAS_PV_INSTALLATION_TYPE_FACADE_INTEGRATED_NO_AIRING,
} IsasPvInstallationType;


/**
 * isas_pv_current_short_circuit_dep_temp:
 * @I_sc_stc: the short circuit current for standard test conditions (STC)
 * @I_sc_temp_coefficient: temperature coefficient for the short circuit current in %/K (Percent / Kelvin). Normally the the coefficient is possitive.
 * @module_temperature: the temperature of the pv module (in deg celsius)
 * @E_glob_tilt: irradiance in W/(m*m)
 *
 * Calculates the short circuit current for a given temperature and a given irradiance for a PV module.
 * see: Volker Quasching, Regenative Energiesysteme, Auflage 5, Seite 171
 *
 * Returns: the short circuit current for the new temperature with 1000 W/(m*m) irradiance
 */
gdouble isas_pv_current_short_circuit_dep_temp (gdouble I_sc_stc, gdouble I_sc_temp_coefficient,
											   gdouble module_temperature, gdouble E_glob_tilt);


/**
 * isas_pv_voltage_open_circuit_dep_temp:
 * @U_oc_stc: the open circuit voltage for standard test conditions (STC)
 * @U_oc_temp_coefficient: temperature coefficient for the open circuit voltage in %/K (Percent / Kelvin). Normally the coefficient is negative.
 * @module_temperature: the temperature of the pv module (in deg celsius)
 * @E_glob_tilt: irradiance in W/(m*m)
 *
 * Calculates the open circuit voltage for a given temperature for a PV module.
 * See: Volker Quasching, Regenative Energiesysteme, Auflage 5, Seite 171
 *
 * Returns: the open circuit voltage for the new temperature with 1000 W/(m*m) irradiance
 */
gdouble isas_pv_voltage_open_circuit_dep_temp (gdouble U_oc_stc, gdouble U_oc_temp_coefficient,
											   gdouble module_temperature, gdouble E_glob_tilt);


/**
 * isas_pv_power_dep_temp:
 * @P_mpp_stc: the power (in Maximum Power Point) for standard test conditions (STC)
 * @P_temp_coefficient: the temperature coefficient for the module in %/K (Percent / Kelvin)
 * @module_temperature: the temperature of the module (in deg celsius)
 * @E_glob_tilt: irradiance in W/(m*m)
 *
 * Calculates the Power (in Watt) for the given temperature from a PV module.
 *
 * @return The Power (in Watt) for the Modul by the given Temperature with 1000 W/(m*m) irradiance
 */
gdouble isas_pv_power_dep_temp (gdouble P_mpp_stc, gdouble P_temp_coefficient,
								gdouble module_temperature, gdouble E_glob_tilt);


/**
 * isas_pv_module_temperature:
 * @ambient_temp: the ambient temperature (in deg Celsius)
 * @E_glob_tilt: the global irradiation in W/(m*m)
 * @installation_type: The Installation type of the pv module
 *
 * Simple approximation of the pv module temperature depending on the irradiation and ambient temperature
 * Montageart                                          Temperaturerhöhung ∆θ
 * 0: Völlig freie Auständerung                        22 °C
 * 1: Auf dem Dach, großer Abstand                     28 °C
 * 2: Auf/im Dach, gute Hinterlüftung                  29 °C
 * 3: Auf/im Dach, schlechte Hinterlüftung             32 °C
 * 4: Auf/in Fassade, gute Hinterlüftung               35 °C
 * 5: Auf/in Fassade, schlechte Hinterlüftung          39 °C
 * 6: Dachintegration, ohne Hinterlüftung              43 °C
 * 7: Fassadenintegration, ohne Hinterlüftung          55 °C
 *
 * Returns: the module temperature (in deg Celsius)
 * See: Lecture from Volker Quaschning, FHTW Berlin, Bachelor Renewable Energy Systems, 2005
 *
 * <note>
 * <para>
 *   This is a very simple approximation. Use with care!
 * </para>
 * </note>
 */
gdouble isas_pv_module_temperature (gdouble ambient_temp, gdouble E_glob_tilt,
									IsasPvInstallationType installation_type );


/**
 * isas_pv_newton_U:
 * @U_startvalue: the start value for U (needed because numeric newton calculation)
 * @I_v: the given current (in Ampere)
 * @border_difference: the value to stop the calculation (could be 1E-6 for example)
 * @m1: diode factor (if you don't know, good Value is 1)
 * @m2: diode factor (if you don't know, good Value is 2)
 * @U_T: temperature voltage (in Volt)
 * @I_Ph: photo current (in Ampere)
 * @I_S1: saturation current (in Ampere)
 * @I_S2: saturation current (in Ampere)
 * @R_S: seriell resistor (in Ohm)
 * @R_P: parallel resistor (in Ohm)
 *
 * Calculates the voltage (in Volt) by a given current (in Ampere)
 *
 * Returns: the voltage (in Volt) for the given current I_v
 */
gdouble isas_pv_newton_U (gdouble U_startvalue, gdouble I_v, gdouble border_difference,
						  gdouble m1, gdouble m2, gdouble U_T, gdouble I_Ph,
						  gdouble I_S1, gdouble I_S2, gdouble R_S, gdouble R_P);


/**
 * isas_pv_newton_I:
 * @U_v: the given voltage (in Volt)
 * @I_startvalue: the startvalue for I (needed because numeric newton calculation)
 * @border_difference: the value to stop the calculation (could be 1E-6 for example)
 * @m1: diode factor (if you don't know, good Value is 1)
 * @m2: diode factor (if you don't know, good Value is 2)
 * @U_T: temperature voltage (in Volt)
 * @I_Ph: photo current (in Ampere)
 * @I_S1: saturation current (in Ampere)
 * @I_S2: saturation current (in Ampere)
 * @R_S: seriell resistor (in Ohm)
 * @R_P: parallel resistor (in Ohm)
 *
 * Calculates the current(in Ampere) by a given Voltage (in Volt)
 *
 * Returns: the voltage (in Volt) for the given current I_v
 */
gdouble isas_pv_newton_I (gdouble U_v, gdouble I_startvalue, gdouble border_difference,
						  gdouble m1, gdouble m2, gdouble U_T, gdouble I_Ph,
						  gdouble I_S1, gdouble I_S2, gdouble R_S, gdouble R_P);


/**
 * isas_pv_U_open_circuit:
 * @U_startvalue: the startvalue for U (needed because numeric newton calculation)
 * @border_difference: the value to stop the valculation (could be 1E-6 for example)
 * @m1: diode factor (if you don't know, good Value is 1)
 * @m2: diode factor (if you don't know, good Value is 2)
 * @U_T:temperature voltage (in Volt)
 * @I_Ph: photo current (in Ampere)
 * @I_S1: saturation current (in Ampere)
 * @I_S2: saturation current (in Ampere)
 * @R_S: seriell resistor (in Ohm)
 * @R_P: parallel resistor (in Ohm)
 *
 * Calculate the open circuit voltage (in Volt)
 *
 * Returns: the open circuit voltage (in Volt)
 */
gdouble isas_pv_U_open_circuit (gdouble U_startvalue, gdouble border_difference,
								gdouble m1, gdouble m2, gdouble U_T, gdouble I_Ph,
								gdouble I_S1, gdouble I_S2, gdouble R_S, gdouble R_P);


/**
 * isas_pv_I_short_circuit:
 * @I_startvalue: The startvalue for I (needed because numeric newton calculation)
 * @border_difference: the value to stop the valculation (could be 1E-6 for example)
 * @m1: diode factor (if you don't know, good Value is 1)
 * @m2: diode factor (if you don't know, good Value is 2)
 * @U_T:temperature voltage (in Volt)
 * @I_Ph: photo current (in Ampere)
 * @I_S1: saturation current (in Ampere)
 * @I_S2: saturation current (in Ampere)
 * @R_S: seriell resistor (in Ohm)
 * @R_P: parallel resistor (in Ohm)
 *
 * Calculates the short circuit current (in Ampere)
 *
 * Returns: the short circuit current (in Ampere)
 */
gdouble isas_pv_I_short_circuit (gdouble I_startvalue, gdouble border_difference,
								 gdouble m1, gdouble m2, gdouble U_T, gdouble I_Ph,
								 gdouble I_S1, gdouble I_S2, gdouble R_S, gdouble R_P);


/**
 * isas_pv_module_characteristic_curve:
 * @U_steps: Number of steps from U=0V to U=U_L ()
 * @U_startvalue: the startvalue for U (needed because numeric newton calculation)
 * @border_difference: the value to stop the calculation (could be 1E-6 for example)
 * @m1: diode factor for D1 (if you don't know, use Value 1)
 * @m2: diode factor for D2 (if you don't know, use Value 2)
 * @U_T:temperature voltage (in Volt)
 * @I_Ph: photo current (in Ampere)
 * @I_S1: saturation current (in Ampere)
 * @I_S2: saturation current (in Ampere)
 * @R_S: seriell resistor (in Ohm)
 * @R_P: parallel resistor (in Ohm)
 * @res_U: (element-type gdouble) (array) (out) (transfer full): array with voltage values
 * @res_I: (element-type gdouble) (array) (out) (transfer full): array with current values
 * @U_MPP: (out): voltage maximum power point
 * @I_MPP: (out): current maximum power point
 *
 * Get the characteristic Curve for the SolarCell with numeric newton calculation. Caller must free #res_U and #res_I with g_array_free().
 * <link linkend="IsasPVTwoDiodeModel">
 *  <inlinegraphic fileref="two-diode-model.png" format="PNG" />
 * </link>
 *
 * Example values are: m1:1 ; m2:2 ; U_T:0.0257 V ; I_Ph:3.17 A ; I_S1:0.00000000034 A ; 
 *                     I_S2:0.0000077 ; R_S:0.015 Ohm ; R_P:155 Ohm
 */
void isas_pv_module_characteristic_curve (gint U_steps, gdouble U_startvalue, gdouble border_difference,
										  gdouble m1, gdouble m2, gdouble U_T, gdouble I_Ph,
										  gdouble I_S1, gdouble I_S2, gdouble R_S, gdouble R_P,
										  GArray** res_U, GArray** res_I, gdouble *U_MPP, gdouble *I_MPP);


G_END_DECLS

#endif /* _ISAS_PV_H_ */
