/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * This file is part of ISAS.
 *
 * Copyright (C) 2011 Thomas Bechtold <thomasbechtold@jpberlin.de>
 *
 * ISAS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * ISAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with ISAS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "isas-pv.h"


gdouble 
isas_pv_current_short_circuit_dep_temp (gdouble I_sc_stc, gdouble I_sc_temp_coefficient, 
										gdouble module_temperature, gdouble E_glob_tilt)
{
    return I_sc_stc * (E_glob_tilt/1000.0) * ( 1.0 + I_sc_temp_coefficient * (module_temperature - 25.0));
}


gdouble 
isas_pv_voltage_open_circuit_dep_temp (gdouble U_oc_stc, gdouble U_oc_temp_coefficient, 
									   gdouble module_temperature, gdouble E_glob_tilt)
{
    return U_oc_stc * (E_glob_tilt/1000.0) * ( 1.0 + U_oc_temp_coefficient * (module_temperature - 25.0));
}
 

gdouble 
isas_pv_power_dep_temp (gdouble P_mpp_stc, gdouble P_temp_coefficient,
					   gdouble module_temperature, gdouble E_glob_tilt)
{
    return P_mpp_stc * (E_glob_tilt/1000.0) * ( 1.0 + P_temp_coefficient * (module_temperature - 25.0));
}

  
gdouble 
isas_pv_module_temperature (gdouble ambient_temp, gdouble E_glob_tilt, 
				   IsasPvInstallationType installation_type )
{ 
    gint d_theta = 22;
        
	if(installation_type == 0)
		d_theta = 22;
	else if(installation_type == 1)
		d_theta = 28;
	else if(installation_type == 2)
		d_theta = 29;
	else if(installation_type == 3)
		d_theta = 32;
	else if(installation_type == 4)
		d_theta = 35;
	else if(installation_type == 5)
		d_theta = 39;
	else if(installation_type == 6)
		d_theta = 43;
	else if(installation_type == 7)
		d_theta = 55;
	else
		g_warning ("installation type '%i' not available.", installation_type);

    return ambient_temp + ((E_glob_tilt / 1000.0) * d_theta);
}


/**
 * isas_pv_deltafUI_deltaI:
 * @U: voltage
 * @I: current
 * @m1: diode factor (if you don't know, good Value is 1)
 * @m2: diode factor (if you don't know, good Value is 2)
 * @U_T: temperature voltage (in Volt)
 * @I_S1: saturation current (in Ampere)
 * @I_S2: saturation current (in Ampere)
 * @R_S: seriell resistor (in Ohm)
 * @R_P: parallel resistor (in Ohm)
 *
 * Calculates the derivation of the implicit equation for the 2-Dioden-Model df(U,I)/DI
 *
 * Returns: TODO
 */
static gdouble 
isas_pv_deltafUI_deltaI (gdouble U, gdouble I, gdouble m1, gdouble m2, 
				       gdouble U_T, gdouble I_S1, gdouble I_S2, 
				       gdouble R_S, gdouble R_P)
{
    return -I_S1 * exp( (U + I * R_S)/(m1 * U_T) ) * R_S / (m1*U_T) - I_S2 * 
	exp( (U+I*R_S)/(m2*U_T) ) * R_S / (m2*U_T) - (R_S/(R_P)) -1 ;
}


/**
 * isas_pv_deltafUI_deltaU:
 * @U: voltage
 * @I: current
 * @m1: diode factor (if you don't know, good Value is 1)
 * @m2: diode factor (if you don't know, good Value is 2)
 * @U_T: temperature voltage (in Volt)
 * @I_S1: saturation current (in Ampere)
 * @I_S2: saturation current (in Ampere)
 * @R_S: seriell resistor (in Ohm)
 * @R_P: parallel resistor (in Ohm)
 *
 * Calculates the derivation of the implicit equation for the 2-Dioden-Model df(U,I)/DU
 *
 * Returns: TODO
 */
static gdouble 
isas_pv_deltafUI_deltaU (gdouble U, gdouble I, gdouble m1, gdouble m2,
				       gdouble U_T, gdouble I_S1, gdouble I_S2, 
				       gdouble R_S, gdouble R_P)
{	
    return -I_S1 * exp( (U + I * R_S)/(m1 * U_T) ) * 1 / (m1*U_T) - I_S2 * 
	exp( (U+I*R_S)/(m2*U_T) ) * 1 / (m2*U_T) - (1/(R_P)) ;
}


gdouble 
isas_pv_newton_U (gdouble U_startvalue, gdouble I_v, gdouble border_difference, 
			 gdouble m1, gdouble m2, gdouble U_T, gdouble I_Ph,
			 gdouble I_S1, gdouble I_S2, gdouble R_S, gdouble R_P)
{
    gdouble U_i1, U_i2;
    gdouble border = 0;
    U_i1 = U_startvalue;
    gint counter = 0;
		
    U_i2 = U_i1 - ( (I_Ph - I_S1 * (exp((U_i1+I_v*R_S)/(m1*U_T)) -1) - I_S2 * (exp((U_i1 + I_v*R_S)/(m2*U_T)) - 1) - (U_i1 + I_v*R_S)/R_P - I_v) / (isas_pv_deltafUI_deltaU(U_i1, I_v,m1,m2,U_T,I_S1,I_S2,R_S,R_P)) );
	
    border = sqrt(pow(U_i2 - U_i1, 2));
	
    while (border >= border_difference && counter < 100000)
    {
		U_i2 = U_i1 - ( (I_Ph - I_S1 * (exp((U_i1+I_v*R_S)/(m1*U_T)) -1) - I_S2 * (exp((U_i1 + I_v*R_S)/(m2*U_T)) - 1) - (U_i1 + I_v*R_S)/R_P - I_v    ) / (isas_pv_deltafUI_deltaU(U_i1, I_v,m1,m2,U_T,I_S1,I_S2,R_S,R_P)) );
		border = sqrt(pow(U_i2 - U_i1, 2));
		U_i1 = U_i2;
		counter++;
    }	
    return U_i2;
}


gdouble 
isas_pv_newton_I (gdouble U_v, gdouble I_startvalue, gdouble border_difference, 
				  gdouble m1, gdouble m2, gdouble U_T, gdouble I_Ph, 
				  gdouble I_S1, gdouble I_S2, gdouble R_S, gdouble R_P)
{
    gdouble I_i1, I_i2;
    gdouble border = 0;
    gint counter = 0;
    I_i1 = I_startvalue;
		
    I_i2 = I_i1 - ( (I_Ph - I_S1 * (exp((U_v+I_i1*R_S)/(m1*U_T)) -1) - I_S2 * (exp((U_v + I_i1*R_S)/(m2*U_T)) - 1) - (U_v + I_i1*R_S)/R_P - I_i1) / (isas_pv_deltafUI_deltaI(U_v, I_i1,m1,m2,U_T,I_S1,I_S2,R_S,R_P)) );
		
    border = sqrt(pow(I_i2 - I_i1, 2));
		
    while (border >= border_difference && counter < 100000)
    {
		I_i2 = I_i1 - ( (I_Ph - I_S1 * (exp((U_v+I_i1*R_S)/(m1*U_T)) -1) - I_S2 * (exp((U_v + I_i1*R_S)/(m2*U_T)) - 1) - (U_v + I_i1*R_S)/R_P - I_i1) / (isas_pv_deltafUI_deltaI(U_v, I_i1,m1,m2,U_T,I_S1,I_S2,R_S,R_P)) );
		border = sqrt(pow(I_i2 - I_i1, 2));
		I_i1 = I_i2;
		counter++;
    }	
    return I_i2;
}


gdouble 
isas_pv_U_open_circuit (gdouble U_startvalue, gdouble border_difference, 
						gdouble m1, gdouble m2, gdouble U_T, gdouble I_Ph, 
						gdouble I_S1, gdouble I_S2, gdouble R_S, gdouble R_P)
{
    return isas_pv_newton_U (U_startvalue, 0, border_difference, m1, m2, U_T, I_Ph, I_S1, I_S2, R_S, R_P);
}
    

gdouble 
isas_pv_I_short_circuit (gdouble I_startvalue, gdouble border_difference, 
						 gdouble m1, gdouble m2, gdouble U_T, gdouble I_Ph, 
						 gdouble I_S1, gdouble I_S2, gdouble R_S, gdouble R_P)
{
    return isas_pv_newton_I (0,I_startvalue, border_difference, m1,m2,U_T,I_Ph,I_S1,I_S2,R_S,R_P);
}


typedef struct {
	gdouble U;
	gdouble I;
} IsasPvUI;


void 
isas_pv_module_characteristic_curve (gint U_steps, gdouble U_startvalue, gdouble border_difference, 
									 gdouble m1, gdouble m2, gdouble U_T, gdouble I_Ph, 
									 gdouble I_S1, gdouble I_S2, gdouble R_S, gdouble R_P,
									 GArray** res_U, GArray** res_I, gdouble *U_MPP, gdouble *I_MPP)
{
	gdouble U_OC;
	gdouble I_SC;
	gdouble u_tmp;
	gdouble U_stepsize;
	gdouble P_MPP = 0;
	*U_MPP = 0;
	*I_MPP = 0;

	*res_U = g_array_new (FALSE, FALSE, sizeof (gdouble));
	*res_I = g_array_new (FALSE, FALSE, sizeof (gdouble));
	
	U_OC = isas_pv_U_open_circuit (U_startvalue, border_difference, m1, m2, U_T,I_Ph, I_S1, I_S2, R_S, R_P);
	I_SC = 0.0;
	U_stepsize = U_OC / U_steps;


	for(u_tmp = 0.0; u_tmp < U_OC; u_tmp = u_tmp + U_stepsize)
	{
		gdouble i_tmp = isas_pv_newton_I (u_tmp, I_Ph, border_difference, m1, m2, U_T,I_Ph, I_S1, I_S2, R_S, R_P);
		//check for new MPP
		if ((u_tmp * i_tmp) > P_MPP)
		{
			P_MPP = u_tmp*i_tmp;
			*U_MPP = u_tmp;
			*I_MPP = i_tmp;
		}
		//append values
		g_array_append_val (*res_U, u_tmp);
		g_array_append_val (*res_I, i_tmp);
	}
	g_array_append_val (*res_U, U_OC);
	g_array_append_val (*res_I, I_SC);
}
