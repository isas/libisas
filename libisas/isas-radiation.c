/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * This file is part of ISAS.
 *
 * Copyright (C) 2011 Thomas Bechtold <thomasbechtold@jpberlin.de>
 *
 * ISAS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * ISAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with ISAS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>

#include "isas-tools.h"
#include "isas-sky.h"



void 
isas_radiation_E_global_hor_TO_E_direct_hor_and_E_diffuse_hor (gdouble solarconstant, gdouble sun_height,
							  gdouble E_global_hor, gdouble* E_direct_hor, gdouble* E_diffuse_hor)
{
	g_return_if_fail (E_direct_hor != NULL);
	g_return_if_fail (E_diffuse_hor != NULL);

    gdouble kt = -1;
    
    *E_direct_hor = 0;
    *E_diffuse_hor = 0;
    
    if (E_global_hor > 0)
    {
		kt = E_global_hor / ( solarconstant * sin (isas_tools_degrees_to_radians (sun_height)));
		if (kt <= 0.3)
		{
			*E_diffuse_hor = E_global_hor * ( 1.020 - (0.254 * kt) + (0.0123 * sin (isas_tools_degrees_to_radians (sun_height))));
            // Constraint: E_diffuse_hor/E_global_hor < 1.0
			*E_diffuse_hor = MIN (*E_diffuse_hor, E_global_hor);
			
		} 
		else if (kt > 0.3 && kt < 0.78)
		{
			*E_diffuse_hor = E_global_hor * ( 1.400 - (1.749 * kt) + (0.177 * sin (isas_tools_degrees_to_radians (sun_height))));
			//Constraint: E_diffuse_hor/E_global_hor < 0.97 and E_diffuse_hor/E_global_hor > O.l
			*E_diffuse_hor = MIN (*E_diffuse_hor, E_global_hor*0.97);
			*E_diffuse_hor = MAX (*E_diffuse_hor, E_global_hor*0.1);
		}
		else
		{
			*E_diffuse_hor = E_global_hor * ( (0.486 * kt) - (0.182 * sin (isas_tools_degrees_to_radians (sun_height))));
			//Constraint: E_diffuse_hor/E_global_hor > O.l.
			*E_diffuse_hor = MAX (*E_diffuse_hor, E_global_hor*0.1);
		}
		*E_direct_hor = E_global_hor - *E_diffuse_hor;
    }
}


gdouble 
isas_radiation_E_direct_hor_TO_E_direct_tilt (gdouble sun_height, gdouble sun_azimuth, gdouble E_direct_hor, gdouble plain_height, gdouble plain_azimuth)
{
    gdouble E_direct_tilt = 0.0;

	//plausability check
    if (sun_height <= 0 || E_direct_hor <= 0) 
		return 0.0;
    
    //Angle between the Normalvector of the Sun and the normalvector of the plain
    gdouble angle_sunnormal_plainnormal = isas_tools_angle_sunnormal_plainnormal (sun_height, sun_azimuth, plain_height, plain_azimuth);
    
    //Result
    E_direct_tilt = E_direct_hor * (cos (isas_tools_degrees_to_radians (angle_sunnormal_plainnormal))) /
		(sin (isas_tools_degrees_to_radians(sun_height)));
    
    //Correct E_dir_tilt if < 0 because angle between sunNormal and plainNormal is > 90
    if (E_direct_tilt < 0) E_direct_tilt = 0;
    
    return E_direct_tilt;
}


gdouble 
isas_radiation_E_refl_tilt (gdouble E_global_hor, gdouble albedo, gdouble plain_height)
{
	return E_global_hor * albedo * 0.5 * (1.0 - cos (isas_tools_degrees_to_radians (plain_height)));
}


gdouble 
isas_radiation_E_diffuse_hor_TO_E_diffuse_tilt (gdouble solarconstant, gdouble sun_height, gdouble sun_azimuth, double E_direct_hor,
												double E_diffuse_hor, gdouble plain_height, gdouble plain_azimuth)
{
    //The return Value
    gdouble E_diffuse_tilt = 0;

    //Factors from Perez-Model
    gdouble sky_clearness, sky_brightness;
    gdouble F11, F12, F13, F21, F22, F23;
    gdouble F1, F2;
    gdouble a,b;
    gdouble angle_sunnormal_plainnormal = -1;
    gdouble airmass;

    gdouble sun_zenith = 90.0 - sun_height;

    airmass = isas_tools_airmass (sun_height);
    sky_clearness = isas_sky_clearness (sun_height, E_direct_hor, E_diffuse_hor);
    sky_brightness = isas_sky_brightness (solarconstant, sun_height, E_diffuse_hor);
    
    if (sky_clearness >= 1.000 && sky_clearness <1.065)
    {
	F11 = -0.008;
	F12 = 0.588;
	F13 = -0.062;
	F21 = -0.060;
	F22 = 0.072;
	F23 = -0.022;
    }
    else if (sky_clearness >= 1.065 && sky_clearness <1.230)
    {
	F11 = 0.130;
	F12 = 0.683;
	F13 = -0.151;
	F21 = -0.019;
	F22 = 0.066;
	F23 = -0.029;
	}
    else if (sky_clearness >= 1.230 && sky_clearness <1.500)
    {
	F11 = 0.330;
	F12 = 0.487;
	F13 = -0.221;
	F21 = 0.055;
	F22 = -0.064;
	F23 = -0.026;
    }
    else if (sky_clearness >= 1.500 && sky_clearness <1.950)
    {
	F11 = 0.568;
	F12 = 0.187;
	F13 = -0.295;
	F21 = 0.109;
	F22 = -0.152;
	F23 = -0.014;
    }
    else if (sky_clearness >= 1.950 && sky_clearness <2.800)
    {
	F11 = 0.873;
	F12 = -0.392;
	F13 = -0.362;
	F21 = 0.226;
	F22 = -0.462;
	F23 = 0.001;
    }
    else if (sky_clearness >= 2.800 && sky_clearness <4.500)
    {
	F11 = 1.132;
	F12 = -1.237;
	F13 = -0.412;
	F21 = 0.288;
	F22 = -0.823;
	F23 = 0.056;
    }
    else if (sky_clearness >= 4.500 && sky_clearness <6.200)
    {
	F11 = 1.060;
	F12 = -1.600;
	F13 = -0.359;
	F21 = 0.264;
	F22 = -1.127;
	F23 = 0.131;
    }
    else if (sky_clearness > 6.200)
    {
	F11 = 0.678;
	F12 = -0.327;
	F13 = -0.250;
	F21 = 0.156;
	F22 = -1.377;
	F23 = 0.251;
    } 
    else
    {
	g_warning ("sky_clearness %5f out of range.", sky_clearness);
	F11 = 0;
	F12 = 0;
	F13 = 0;
	F21 = 0;
	F22 = 0;
	F23 = 0;
    }

    angle_sunnormal_plainnormal = isas_tools_angle_sunnormal_plainnormal (sun_height, sun_azimuth, plain_height, plain_azimuth);

    F1 = F11 + F12*sky_brightness + F13 * isas_tools_degrees_to_radians (sun_zenith);
    F2 = F21 + F22*sky_brightness + F23 * isas_tools_degrees_to_radians (sun_zenith);
    a = MAX (0, cos (isas_tools_degrees_to_radians (angle_sunnormal_plainnormal)));
    b = MAX (0.087, sin (isas_tools_degrees_to_radians (sun_height)));

    //Result
    E_diffuse_tilt = E_diffuse_hor * (0.5 * (1 + cos(isas_tools_degrees_to_radians (plain_height))) *
									  (1 - F1) + (a / b) * F1 + F2 * sin (isas_tools_degrees_to_radians (plain_height)));

    return E_diffuse_tilt;
}
