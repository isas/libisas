/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * This file is part of ISAS.
 *
 * Copyright (C) 2011 Thomas Bechtold <thomasbechtold@jpberlin.de>
 *
 * ISAS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * ISAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with ISAS.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _ISAS_RADIATION_H_
#define _ISAS_RADIATION_H_


/**
 * SECTION:isas-radiation
 * @short_description: methods to calculate direct and diffuse radiation for horizontal and tilt plains.
 * @title: isas-radiation
 * @section_id:
 * @stability: Unstable
 * @include: isas-radiation.h
 *
 * The methods described below can convert (with different models) global radiaton to direct and diffuse radiation.
 * It's alo possible to convert from a horizontal plain to a tilt plain.
 */



G_BEGIN_DECLS


/**
 * isas_radiation_E_global_hor_TO_E_direct_hor_and_E_diffuse_hor:
 * @solarconstant: the solar constant (in W/(m*m))
 * @sun_height: sun height in degrees (0 < point_height <= 90 ; 90 degrees is zenith)
 * @E_global_hor: Global horizontal irradiation (in W/(m*m))
 * @E_direct_hor: the direct horizontal irradiation (in W/(m*m))
 * @E_diffuse_hor: the diffuse horizontal irradiation (in W/(m*m))
 *
 * Calculates from the global horizontal irradiation for the given sun height the direct and diffuse Part.
 * See: Reindl, D.T.; Beckmann, W.A.; Duffie J.A. : Diffuse Fraction Correlations.
 *      In: Proceedings of ISES Solar World Conference 1989, Page 2082-2086
 */
void isas_radiation_E_global_hor_TO_E_direct_hor_and_E_diffuse_hor (gdouble solarconstant, 
																	gdouble sun_height, 
																	gdouble E_global_hor, 
																	gdouble* E_direct_hor, 
																	gdouble* E_diffuse_hor);

/**
 * isas_radiation_E_direct_hor_TO_E_direct_tilt:
 * @sun_height: sun height in degrees (0 < point_height <= 90 ; 90 degrees is zenith)
 * @sun_azimuth: sun azimut in degrees (0 degree -> north, 90 degrees -> east, 180 degrees -> south, 270 degrees -> west)
 * @E_direct_hor:irradiation on the horizontal plain (in W/(m*m) )
 * @plain_height: angle of the plain in degrees. 0 degrees -> plain looks to the zenith ; 90 degrees -> plain looks to the horizont
 * @plain_azimuth: azimutangle of the plain in degrees. 0 degree -> Plain look to South ; 90 degree -> look to East ; 180 degree -> to North ; 270 degree -> 270 degree
 *
 * Returns: irradiation on the tilt plain (in W/(m*m))
 */
gdouble isas_radiation_E_direct_hor_TO_E_direct_tilt (gdouble sun_height, gdouble sun_azimuth, 
													  gdouble E_direct_hor, gdouble plain_height, 
													  gdouble plain_azimuth);

/**
 * isas_radiation_E_refl_tilt:
 * @E_global_hor: the global horizontal irradiation in W/(m*m)
 * @albedo: the albedo-value (if unavailable, use 0,2)
 * @plain_height: angle of the plain in degrees. 0 degrees -> plain looks to the zenith ; 90 degrees -> plain looks to the horizont
 *
 * Calculates the Reflection from the Ground if a Plain is tilt.
 *
 * Returns: the reflected irradiation in W/(m*m)
 */
gdouble isas_radiation_E_refl_tilt (gdouble E_global_hor, gdouble albedo, gdouble plain_height);


/**
 * isas_radiation_E_diffuse_hor_TO_E_diffuse_tilt:
 * @solarconstant: the solarconstant (in W/(m*m))
 * @sun_height: sun height in degrees (0 < point_height <= 90 ; 90 degrees is zenith)
 * @sun_azimuth: sun azimut in degrees (0 degree -> north, 90 degrees -> east, 180 degrees -> south, 270 degrees -> west)
 * @E_direct_hor: direct irradiation on the horizontal plain (in W/(m*m) )
 * @E_diffuse_hor: diffuse irradiation on the horizontal plain (in W/(m*m) )
 * @plain_height: angle of the plain in degrees. 0 degrees -> plain looks to the zenith ; 90 degrees -> plain looks to the horizont
 * @plain_azimuth: azimutangle of the plain in degrees. 0 degree -> Plain look to South ; 90 degree -> look to East ; 180 degree -> to North ; 270 degree -> 270 degree
 *
 * Calculates from the Diffuse horizontal Irradiation the Diffuse tilted Irradiation ( in W/(m*m) ) with the model from Perez.
 *
 * Returns: diffuse irradiance for a tilted plain ( in W/(m*m) )
 */
gdouble isas_radiation_E_diffuse_hor_TO_E_diffuse_tilt (gdouble solarconstant, gdouble sun_height, gdouble sun_azimuth, double E_direct_hor,
														double E_diffuse_hor, gdouble plain_height, gdouble plain_azimuth);


/**
 * isas_radiation_luminance_to_radiance:
 * @luminance: the luminance in cd/(m²)
 *
 * Transform from luminance to radiance (DIN 5034)
 * see See http://volker-quaschning.de/downloads/abschattungsverluste.pdf page 106
 *
 * Returns: the radiance in W/(m²*sr)
 */
gdouble isas_radiation_luminance_to_radiance (gdouble luminance);


G_END_DECLS

#endif /* _ISAS_RADIATION_H_ */
