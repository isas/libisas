/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * This file is part of ISAS.
 *
 * Copyright (C) 2011 Thomas Bechtold <thomasbechtold@jpberlin.de>
 *
 * ISAS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * ISAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with ISAS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ISAS_TOOLS_H_
#define _ISAS_TOOLS_H_

#include <glib.h>


gdouble isas_tools_degrees_to_radians(gdouble deg);

gdouble isas_tools_radians_to_degrees(gdouble rad);

gdouble isas_tools_solarconstant(GDateTime* dt);

gdouble isas_tools_airmass(gdouble sun_height);

gdouble isas_tools_angle_sun_skypoint(gdouble sun_height, gdouble sun_azimuth, gdouble point_height, gdouble point_azimuth);

gdouble isas_tools_angle_sunnormal_plainnormal(gdouble sun_height, gdouble sun_azimuth, gdouble plain_height, gdouble plain_azimuth);

gdouble isas_tools_luminance_TO_irradiance(gdouble luminance);



#endif /* _ISAS_TOOLS_H_ */
