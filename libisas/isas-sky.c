/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/*
 * This file is part of ISAS.
 *
 * Copyright (C) 2011 Thomas Bechtold <thomasbechtold@jpberlin.de>
 *
 * ISAS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with ISAS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>

#include "isas-tools.h"

#define KAPPA 1.041


void 
isas_sky_sunpos_DIN5034 (gdouble longitude, gdouble latitude, gint year, gint month, gint day, gint hour, gint minute, gint second, gchar* timezone, gdouble* sun_height, gdouble* sun_azimuth)
{
	g_return_if_fail (sun_height != NULL);
	g_return_if_fail (sun_azimuth != NULL);

    GTimeZone* tz = g_time_zone_new (timezone);
    if(!tz)
    {
		g_warning ("can not create timezone object for '%s'", timezone);
		return;
    }

    GDateTime* dt = g_date_time_new (tz, year, month, day, hour, minute, second); //time is UTC
    if(!dt)
    {
		g_warning ("can not create datetime object.");
		g_time_zone_unref (tz);
		return;
    }

    gdouble MOZ, declination, timeequation, WOZ, w, asinGs, acosAs;
    
    gdouble J, J2;
    J = g_date_time_get_day_of_year (dt);
    /* Check for leap-Year */
    if(g_date_time_get_year (dt) % 4 == 0)
    {
		J2 = 366.0;
    } 
	else
    {
		J2 = 365.0;
    }
    GTimeSpan utc_offset = g_date_time_get_utc_offset (dt);
    gdouble utc_offset_hour = utc_offset / (1000*1000) / (60*60);

    MOZ = g_date_time_get_hour (dt)
		+ (1.0/60.0 * (gdouble) g_date_time_get_minute (dt))
		+ (1.0/3600.0 * (gdouble) g_date_time_get_second (dt))
		- utc_offset_hour + 1.0;  //+1.0 because it's middle eruopean local time (MOZ)

    MOZ = MOZ - 4.0 * (15.0-longitude) / 60.0;

    J = J * 360.0/J2 + MOZ/24.0;

    declination = 0.3948 - 23.2559*cos(isas_tools_degrees_to_radians (J+9.1))
		- 0.3915*cos(isas_tools_degrees_to_radians (2*J+ 5.4))
		- 0.1764*cos(isas_tools_degrees_to_radians (3*J+26.0));

    timeequation = 0.0066 +  7.3525*cos (isas_tools_degrees_to_radians (J+85.9))
		+  9.9359*cos (isas_tools_degrees_to_radians (2*J+108.9))
		+ 0.3387*cos (isas_tools_degrees_to_radians (3*J+105.2));

    WOZ = MOZ + timeequation / 60.0;
    w = (12.0-WOZ) * 15.0;
    
    asinGs = cos (isas_tools_degrees_to_radians (w)) * cos (isas_tools_degrees_to_radians (latitude)) * cos (isas_tools_degrees_to_radians (declination)) + 
		sin (isas_tools_degrees_to_radians (latitude)) * sin (isas_tools_degrees_to_radians (declination));

    if (asinGs > 1)
		asinGs = 1.0;
    if (asinGs < -1)
		asinGs = -1.0;

    *sun_height = isas_tools_radians_to_degrees (asin (asinGs));

    acosAs = (sin (isas_tools_degrees_to_radians (*sun_height)) * sin (isas_tools_degrees_to_radians (latitude)) - sin (isas_tools_degrees_to_radians (declination))) / 
		(cos (isas_tools_degrees_to_radians (*sun_height)) * cos (isas_tools_degrees_to_radians (latitude)));

    if (acosAs > 1)
		acosAs = 1;
    if (acosAs < -1)
		acosAs = -1;
    *sun_azimuth = isas_tools_radians_to_degrees (acos (acosAs));

    if ((WOZ > 12.0) || (WOZ < 0))
    {
		*sun_azimuth = 180.0 + *sun_azimuth;
    }
	else
    {
		*sun_azimuth = 180.0 - *sun_azimuth;
    }
    g_date_time_unref (dt);
    g_time_zone_unref (tz);
}


gdouble 
isas_sky_brightness (gdouble solarconstant, gdouble sun_height, gdouble E_diffuse_hor)
{
	gdouble airmass = isas_tools_airmass (sun_height);
	return airmass * (E_diffuse_hor / solarconstant);
}


gdouble 
isas_sky_clearness (gdouble sun_height, gdouble E_direct_hor, gdouble E_diffuse_hor)
{
    gdouble sun_zenith = 90.0 - sun_height;
    gdouble clearness = ((E_diffuse_hor + E_direct_hor / sin (isas_tools_degrees_to_radians (sun_height))) /
						 E_diffuse_hor + KAPPA * pow (isas_tools_degrees_to_radians (sun_zenith), 3) / 
						 (1.0 + KAPPA * pow (isas_tools_degrees_to_radians (sun_zenith), 3)));
    return clearness;
}


void
isas_sky_perez_parameter2 (gdouble sky_clearness_index, gdouble* a2, gdouble* b2, gdouble* c2, gdouble* d2)
{
    if(sky_clearness_index >= 1 && sky_clearness_index <= 1.065)
    {
		*a2 = 40.86;
		*b2 = 26.77;
		*c2 = -29.59;
		*d2 = -45.75;
    }
    else if(sky_clearness_index > 1.065 && sky_clearness_index <= 1.23)
    {
		*a2 = 26.58;
		*b2 = 14.73;
		*c2 = 58.46;
		*d2 = -21.25;
    }
    else if(sky_clearness_index > 1.23 && sky_clearness_index <= 1.5)
    {
		*a2 = 19.34;
		*b2 = 2.28;
		*c2 = 100;
		*d2 = 0.25;
    }
    else if(sky_clearness_index > 1.5 && sky_clearness_index <= 1.95)
    {
		*a2 = 13.25;
		*b2 = -1.39;
		*c2 = 124.79;
		*d2 = 15.66;
    }
    else if(sky_clearness_index > 1.95 && sky_clearness_index <= 2.8)
    {
		*a2 = 14.47;
		*b2 = -5.09;
		*c2 = 160.09;
		*d2 = 9.13;
    }
    else if(sky_clearness_index > 2.8 && sky_clearness_index <= 4.5)
    {
		*a2 = 19.76;
		*b2 = -3.88;
		*c2 = 154.61;
		*d2 = -19.21;
    }
    else if(sky_clearness_index > 4.5 && sky_clearness_index <= 6.2)
    {
		*a2 = 28.39;
		*b2 = -9.67;
		*c2 = 151.58;
		*d2 = -69.39;
    }
    else if(sky_clearness_index > 6.2)
    {
		*a2 = 42.91;
		*b2 = -19.62;
		*c2 = 130.80;
		*d2 = -164.08;
    }
}


gdouble
isas_sky_zenith_luminance (gdouble solarconstant, gdouble sun_height, gdouble E_direct_hor, gdouble E_diffuse_hor)
{
    gdouble zl = -1;
    gdouble a2, b2, c2, d2;

    gdouble sky_brightness_index;
    gdouble sky_clearness_index;

    gdouble sun_zenith = 90.0 - sun_height;

    sky_clearness_index = isas_sky_clearness (sun_height, E_direct_hor, E_diffuse_hor);
    sky_brightness_index = isas_sky_brightness (solarconstant, sun_height, E_diffuse_hor);

    isas_sky_perez_parameter2 (sky_clearness_index, &a2, &b2, &c2, &d2);

    zl = E_diffuse_hor * (a2 + b2 * (cos(isas_tools_degrees_to_radians(sun_zenith))) +
						  (c2 * exp(-3 * isas_tools_degrees_to_radians(sun_zenith))) + d2 * sky_brightness_index);
    return zl;
}


void
isas_sky_perez_parameter3 (gdouble sun_height, gdouble sky_clearness_index, gdouble sky_brightness_index,
						   gdouble* a3, gdouble* b3, gdouble* c3, gdouble* d3, gdouble* e3)
{
    gdouble sun_zenith = 90.0 - sun_height;
    gdouble a31 = 0, a32 = 0, a33 = 0, a34 = 0, b31 = 0, b32 = 0, b33 = 0, b34 = 0, c31 = 0, c32 = 0, c33 = 0, 
		c34 = 0, d31 = 0, d32 = 0, d33 = 0, d34 = 0, e31 = 0, e32 = 0, e33 = 0, e34 = 0;
	
    //class 1
    if(sky_clearness_index >= 1 && sky_clearness_index <= 1.065)
    {
		a31 = 1.3525;
		a32 = -0.2576;
		a33 = -0.269;
		a34 = -1.4366;
		b31 = -0.767;
		b32 = 0.0007;
		b33 = 1.2734;
		b34 = -0.1233;
		c31 = 2.8;
		c32 = 0.6004;
		c33 = 1.2375;
		c34 = 1;
		d31 = 1.8734;
		d32 = 0.6297;
		d33 = 0.9738;
		d34 = 0.2809;
		e31 = 0.0356;
		e32 = -0.1246;
		e33 = -0.5718;
		e34 = 0.9938;
    }
    //class 2
    else if(sky_clearness_index > 1.065 && sky_clearness_index <= 1.23)
    {
		a31 = -1.2219;
		a32 = -0.773;
		a33 = 1.4148;
		a34 = 1.1016;
		b31 = -0.2054;
		b32 = 0.0367;
		b33 = -3.9128;
		b34 = 0.9156;
		c31 = 6.975;
		c32 = 0.1774;
		c33 = 6.4477;
		c34 = -0.1239;
		d31 = -1.5798;
		d32 = -0.5081;
		d33 = -1.7812;
		d34 = 0.108;
		e31 = 0.2624;
		e32 = 0.0672;
		e33 = -0.219;
		e34 = -0.4285;
    }
    //class 3
    else if(sky_clearness_index > 1.23 && sky_clearness_index <= 1.5)
    {
		a31 = -1.1;
		a32 = -0.2515;
		a33 = 0.8952;
		a34 = 0.0156;
		b31 = -0.2054;
		b32 = 0.0367;
		b33 = -3.9128;
		b34 = 0.9156;
		c31 = 24.7219;
		c32 = -13.0812;
		c33 = -37.7;
		c34 = 34.8438;
		d31 = -5;
		d32 = 1.5218;
		d33 = 3.9229;
		d34 = -2.6204;
		e31 = -0.0156;
		e32 = 0.1597;
		e33 = 0.4199;
		e34 = -0.5562;
    }
    //class 4
    else if(sky_clearness_index > 1.5 && sky_clearness_index <= 1.95)
    {
		a31 = -0.5484;
		a32 = -0.6654;
		a33 = -0.2672;
		a34 = 0.7117;
		b31 = 0.7234;
		b32 = -0.6219;
		b33 = -5.6812;
		b34 = 2.6297;
		c31 = 33.3389;
		c32 = -18.3;
		c33 = -62.25;
		c34 = 52.0781;
		d31 = -3.5;
		d32 = 0.0016;
		d33 = 1.1477;
		d34 = 0.1062;
		e31 = 0.4659;
		e32 = -0.3296;
		e33 = -0.0876;
		e34 = -0.0329;
    }
    //class 5
    else if(sky_clearness_index > 1.95 && sky_clearness_index <= 2.8)
    {
		a31 = -0.6;
		a32 = -0.3566;
		a33 = -2.5;
		a34 = 2.325;
		b31 = 0.2937;
		b32 = 0.0496;
		b33 = -5.6812;
		b34 = 1.8415;
		c31 = 21;
		c32 = -4.7656;
		c33 = -21.5906;
		c34 = 7.2492;
		d31 = -3.5;
		d32 = -0.1554;
		d33 = 1.4062;
		d34 = 0.3988;
		e31 = 0.0032;
		e32 = 0.0766;
		e33 = -0.0656;
		e34 = -0.1294;
    }
    //class 6
    else if(sky_clearness_index > 2.8 && sky_clearness_index <= 4.5)
    {
		a31 = -1.0156;
		a32 = -0.367;
		a33 = 1.0078;
		a34 = 1.4051;
		b31 = 0.2875;
		b32 = -0.5328;
		b33 = -3.85;
		b34 = 3.375;
		c31 = 14;
		c32 = -0.9999;
		c33 = -7.1406;
		c34 = 7.5469;
		d31 = -3.4;
		d32 = -0.1078;
		d33 = -1.075;
		d34 = 1.5702;
		e31 = -0.0672;
		e32 = 0.4016;
		e33 = 0.3017;
		e34 = -0.4844;
    }
    //class 7
    else if(sky_clearness_index > 4.5 && sky_clearness_index <= 6.2)
    {
		a31 = -1;
		a32 = 0.0211;
		a33 = 0.5025;
		a34 = -0.5119;
		b31 = -0.3;
		b32 = 0.1922;
		b33 = 0.7023;
		b34 = -1.6317;
		c31 = 19;
		c32 = -5;
		c33 = 1.2438;
		c34 = -1.9094;
		d31 = -4;
		d32 = 0.025;
		d33 = 0.3844;
		d34 = 0.2656;
		e31 = 1.0468;
		e32 = -0.3788;
		e33 = -2.4517;
		e34 = 1.4656; 
    }
    //class 8
    else if(sky_clearness_index > 6.2)
    {
		a31 = -1.05;
		a32 = 0.0289;
		a33 = 0.426;
		a34 = 0.359;
		b31 = -0.325;
		b32 = 0.1156;
		b33 = 0.7781;
		b34 = 0.0025;
		c31 = 31.0625;
		c32 = -14.5;
		c33 = -46.1148;
		c34 = 55.375;
		d31 = -7.2312;
		d32 = 0.405;
		d33 = 13.35;
		d34 = 0.6234;
		e31 = 1.5;
		e32 = -0.6426;
		e33 = 1.8564;
		e34 = 0.5636;
    }


    *a3 = a31 + a32 * isas_tools_degrees_to_radians (sun_zenith) + 
        sky_brightness_index * (a33 + a34 * isas_tools_degrees_to_radians (sun_zenith));
    *b3 = b31 + b32 * isas_tools_degrees_to_radians (sun_zenith) + 
        sky_brightness_index * (b33 + b34 * isas_tools_degrees_to_radians (sun_zenith));
    if (sky_clearness_index <= 1.065)
    {
		*c3 = exp(sky_brightness_index * pow (c31 + c32 * isas_tools_degrees_to_radians (sun_zenith), c33)) - c34;
    }
    else
    {
		*c3 = c31 + c32 * isas_tools_degrees_to_radians (sun_zenith) + sky_brightness_index * 
			(c33 + c34 * isas_tools_degrees_to_radians (sun_zenith));
    }

    if (sky_clearness_index <= 1.065) 
    {
		*d3 = -exp(sky_brightness_index * (d31 + d32 * isas_tools_degrees_to_radians (sun_zenith))) + 
			d33 + sky_brightness_index * d34;
    } else
    {
		*d3 = d31 + d32 * isas_tools_degrees_to_radians (sun_zenith) + 
			sky_brightness_index * (d33 + d34 * isas_tools_degrees_to_radians (sun_zenith));
    }

    *e3 = e31 + e32 * isas_tools_degrees_to_radians (sun_zenith) + 
        sky_brightness_index * (e33 + e34 * isas_tools_degrees_to_radians (sun_zenith));
}


gdouble
isas_sky_point_luminance (gdouble sun_height, gdouble sun_azimuth, gdouble zenith_luminance,
						  gdouble point_height, gdouble point_azimuth, double sky_brightness_index,
						  double sky_clearness_index)
{
	gdouble pl;;
	gdouble a3, b3, c3, d3, e3;
	gdouble angle_sun_skypoint;
	gdouble zenithangle_rad;
	double luminance_top = 0;
	double luminance_down = 1;
	
	gdouble sun_zenith = 90.0 - sun_height;
	//Get Perez-Parameter
	isas_sky_perez_parameter3(sun_height, sky_clearness_index, sky_brightness_index, &a3, &b3, &c3, &d3, &e3);

	angle_sun_skypoint = isas_tools_angle_sun_skypoint (sun_height, sun_azimuth, point_height, point_azimuth);
	zenithangle_rad = isas_tools_degrees_to_radians (sun_zenith);

	luminance_top = (1 + a3 * exp (b3 / (sin( isas_tools_degrees_to_radians (point_height))))) * 
		(1 + c3 * exp (d3 * isas_tools_degrees_to_radians (angle_sun_skypoint)) + 
		 e3 * (cos (isas_tools_degrees_to_radians (angle_sun_skypoint)) * 
			   cos (isas_tools_degrees_to_radians (angle_sun_skypoint))));
	luminance_down = ((1 + a3 * exp (b3)) * (1 + c3 * exp (d3 * zenithangle_rad) + e3 * pow (cos (zenithangle_rad),2)));
	/* result */
	pl = zenith_luminance * (luminance_top / luminance_down);
	return pl;
}


gdouble 
isas_sky_luminance_to_radiance (gdouble luminance)
{
	/* K_h = 125.4 cd*sr/W */
	return (1 / 125.4) * luminance;
}




    
