/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/*
 * This file is part of ISAS.
 *
 * Copyright (C) 2011 Thomas Bechtold <thomasbechtold@jpberlin.de>
 *
 * ISAS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with ISAS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ISAS_SKY_H_
#define _ISAS_SKY_H_



/**
 * SECTION:isas-sky
 * @short_description: methods to model the sky and some parameters for the sky.
 * @title: isas-sky
 * @section_id:
 * @stability: Unstable
 * @include: isas-sky.h
 *
 * The methods described below can be used to get sky-parameters and to model the diffuse iradiation distribution.
 * It's alo possible to convert from a horizontal plain to a tilt plain.
 */




G_BEGIN_DECLS

/**
 * isas_sky_sunpos_DIN5034:
 * @longitude: longitude in deg East
 * @latitude: latitude in deg North
 * @year: the year
 * @month: the month
 * @day: the day
 * @hour: the hour
 * @minute: the minute
 * @second: the second
 * @timezone: the timezone (eg "+01:00")
 * @sun_height: (out): sun height in degrees (0 < point_height <= 90 ; 90 degrees is zenith)
 * @sun_azimuth: (out):  sun azimut in degrees (0 degree -> north, 90 degrees -> east, 180 degrees -> south, 270 degrees -> west)
 *
 * Calculate the sunposition (with Din 5034 Part 2 Algorithm)
 */
void isas_sky_sunpos_DIN5034 (gdouble longitude, gdouble latitude, gint year, gint month, gint day, gint hour, gint minute, gint second, gchar* timezone, gdouble* sun_height, gdouble* sun_azimuth);


/**
 * isas_sky_brightness:
 * @solarconstant: the solar constant (in W/(m*m))
 * @sun_height: sun height in degrees (0 < point_height <= 90 ; 90 degrees is zenith)
 * @E_diffuse_hor: diffuse irradiation on the horizontal plain (in W/(m*m) )
 *
 * Calculate the skybrightness index with the model from Perez
 * See: http://volker-quaschning.de/downloads/abschattungsverluste.pdf or "Solar Energie Vol. 50, Number 3, March 1993, pp. 234-245"
 *
 * Returns: The sky brightness
 */
gdouble isas_sky_brightness (gdouble solarconstant, gdouble sun_height, gdouble E_diffuse_hor);


/**
 * isas_sky_clearness:
 * @sun_height: sun height in degrees (0 < point_height <= 90 ; 90 degrees is zenith)
 * @E_direct_hor: direct irradiation on the horizontal plain (in W/(m*m) )
 * @E_diffuse_hor: diffuse irradiation on the horizontal plain (in W/(m*m) )
 *
 * Calculate the skyclearness index with the modem from Perez.
 * see: http://volker-quaschning.de/downloads/abschattungsverluste.pdf or "Solar Energie Vol. 50, Number 3, March 1993, pp. 234-245"
 *
 * Returns: the sky_clearness index
 */
gdouble isas_sky_clearness (gdouble sun_height, gdouble E_direct_hor, gdouble E_diffuse_hor);


/**
 * isas_sky_perez_parameter2:
 * @sky_clearness_index: the sky clearness index
 * @a2: (out): parameter a2
 * @b2: (out): parameter b2
 * @c2: (out): parameter c2
 * @d2: (out): parameter d2
 *
 * Calc the Perez-Parameter (2. from 3 Parameters) needed for the zenith luminance
 * see See http://volker-quaschning.de/downloads/abschattungsverluste.pdf or "Solar Energie Vol. 50, Number 3, March 1993, pp. 234-245"
 *
 */
void isas_sky_perez_parameter2 (gdouble sky_clearness_index, gdouble* a2, gdouble* b2, gdouble* c2, gdouble* d2);


/**
 * isas_sky_zenith_luminance:
 * @solarconstant: the solar constant (in W/(m*m))
 * @sun_height: sun height in degrees (0 < point_height <= 90 ; 90 degrees is zenith)
 * @E_direct_hor: direct irradiation on the horizontal plain (in W/(m*m) )
 * @E_diffuse_hor: diffuse irradiation on the horizontal plain (in W/(m*m) )
 *
 * Calculates the Zenith luminance with a model from Perez.
 * see See http://volker-quaschning.de/downloads/abschattungsverluste.pdf or "Solar Energie Vol. 50, Number 3, March 1993, pp. 234-245"
 *
 * Returns: the zenith luminance
 */
gdouble isas_sky_zenith_luminance (gdouble solarconstant, gdouble sun_height, gdouble E_direct_hor, gdouble E_diffuse_hor);


/**
 * isas_sky_perez_parameter3:
 * @sun_height: sun height in degrees (0 < point_height <= 90 ; 90 degrees is zenith)
 * @sky_clearness_index: Index of sky clearness
 * @sky_brightness_index: Index for sky brightness
 * @a3: (out): parameter a3
 * @b3: (out): parameter b3
 * @c3: (out): parameter c3
 * @d3: (out): parameter d3
 * @e3: (out): parameter e3
 *
 * Calc the Perez-Parameter (3. from 3 Parameters)
 * See: http://volker-quaschning.de/downloads/abschattungsverluste.pdf or "Solar Energie Vol. 50, Number 3, March 1993, pp. 234-245"
 */
void isas_sky_perez_parameter3 (gdouble sun_height, gdouble sky_clearness_index, gdouble sky_brightness_index,  gdouble* a3, gdouble* b3, gdouble* c3, gdouble* d3, gdouble* e3);


/**
 * isas_sky_point_luminance:
 * @sun_height: sun height in degrees (0 < point_height <= 90 ; 90 degrees is zenith)
 * @sun_azimuth: sun azimut in degrees (0 degree -> north, 90 degrees -> east, 180 degrees -> south, 270 degrees -> west)
 * @zenith_luminance: the zenith luminance
 * @point_height: point altitude in degrees (0 < point_height <= 90 ; 90 degrees is zenith)
 * @point_azimuth: point azimut angle in degrees (0 degree -> north, 90 degrees -> east, 180 degrees -> south, 270 degrees -> west)
 * @sky_brightness_index: the index for sky brightness
 * @sky_clearness_index: Index for sky clearness
 * 
 * Calc the Luminance from one specific Skypoint
 *
 * Returns: the luminance for the given sky point
 */
gdouble isas_sky_point_luminance (gdouble sun_height, gdouble sun_azimuth, gdouble zenith_luminance, gdouble point_height, gdouble point_azimuth, double sky_brightness_index, double sky_clearness_index);


G_END_DECLS

#endif /* _ISAS_SKY_H_ */
