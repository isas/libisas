/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * This file is part of ISAS.
 *
 * Copyright (C) 2011 Thomas Bechtold <thomasbechtold@jpberlin.de>
 *
 * ISAS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * ISAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with ISAS.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <glib.h>

#include "../libisas/isas-sky.h"
#include "../libisas/isas-pv.h"
#include "../libisas/isas-radiation.h"


/* the solar constant used for some functions. 
 * the constant normally depends on the date/time and location.
 * But for tests we use just a good default value.
 * the solar constant is the average of the extraterrestrial radiation on earth.
*/
#define SOLAR_CONSTANT 1367.0


/*******************************************************************************
 * Tests for isas-sky functions
 ******************************************************************************/
void test_isas_sky_sunposition (void)
{
	gdouble sun_azimuth = 0;
	gdouble sun_height = 0;

	isas_sky_sunpos_DIN5034(13.5, 52.5, 2002, 3, 31, 12, 30, 0, "+01:00", &sun_height, &sun_azimuth);
	g_assert_cmpfloat(sun_height, >, 41.428142);
	g_assert_cmpfloat(sun_height, <, 41.428143);
	g_assert_cmpfloat(sun_azimuth, >, 186.655472);
	g_assert_cmpfloat(sun_azimuth, <, 186.655473);

	isas_sky_sunpos_DIN5034(13.5, 52.5, 2002,3,31,12,30,0,"+05:00", &sun_height, &sun_azimuth);
	g_assert_cmpfloat(sun_height, >, 23.832185);
	g_assert_cmpfloat(sun_height, <, 23.832187);
	g_assert_cmpfloat(sun_azimuth, >, 116.690041);
	g_assert_cmpfloat(sun_azimuth, <, 116.690043);
}

/*******************************************************************************
 * Tests for isas-pv functions
 ******************************************************************************/
void test_isas_pv_caracteristic_curve (void)
{
	//TODO do something in the testcase
	GArray* res_U = NULL;
	GArray* res_I = NULL;
	gdouble U_MPP, I_MPP;
	isas_pv_module_characteristic_curve(100, 1, 0.0001, 1, 2, 10, 1, 2, 3, 1, 2, &res_U, &res_I, &U_MPP, &I_MPP);
	g_array_free(res_U, TRUE);
	g_array_free(res_I, TRUE);
}


/*******************************************************************************
 * Tests for isas-radiation functions
 ******************************************************************************/
void test_isas_radiation_E_global_hor_TO_E_direct_hor_and_E_diffuse_hor (void)
{
	gdouble direct = 0, diffuse = 0, E_global_hor = 0, sun_height = 0;
	//plausibility checks for diffuse and direct values for different sun-height and radiations
	for (E_global_hor = 0; E_global_hor <= SOLAR_CONSTANT; E_global_hor++)
	{
		for (sun_height = 0; sun_height <= 90; sun_height++)
		isas_radiation_E_global_hor_TO_E_direct_hor_and_E_diffuse_hor (SOLAR_CONSTANT,
																	   sun_height, E_global_hor,
																	   &direct, &diffuse);
		g_assert (direct >= 0);
		g_assert (diffuse >= 0);
		//FIXME: added 0.0001 because of floating point arithmetic
		g_assert (direct + diffuse <= E_global_hor+0.0001);
	}
}

void test_isas_radiation_E_direct_hor_TO_E_direct_tilt (void)
{
	//Just check that E_direct_tilt is always >= 0
	gdouble sun_height, sun_azimuth, plain_height, plain_azimuth, E_direct_hor, E_direct_tilt;
	for (sun_height = 0; sun_height <= 90; sun_height+=15)
	{
		for (sun_azimuth = 0; sun_azimuth <= 360; sun_azimuth+=30)
		{
			for (plain_height = 0; plain_height <= 90; plain_height+=15)
			{
				for (plain_azimuth = 0; plain_azimuth <= 360; plain_azimuth+=30)
				{		
					for (E_direct_hor = 0; E_direct_hor <= SOLAR_CONSTANT; E_direct_hor+=400)
					{
						E_direct_tilt = isas_radiation_E_direct_hor_TO_E_direct_tilt (sun_height,
																					  sun_azimuth,
																					  E_direct_hor,
																					  plain_height,
																					  plain_azimuth);
						//g_message ("sun: %2.f/%2.f ; plain: %2.f/%2.f ; hor/tilt: %2.f/%2.f",
						//		   sun_height,sun_azimuth, plain_height,plain_azimuth,  E_direct_hor,  E_direct_tilt);
						g_assert (E_direct_tilt >= 0);
					}
				}
			}
		}
	}
	

}




/*******************************************************************************
 * entry point for test run
 ******************************************************************************/
int main (int argc, char** argv)
{	
	g_type_init ();
	g_test_init (&argc, &argv, NULL);

    g_test_add_func ("/isas/sky/sunposition", test_isas_sky_sunposition);
    g_test_add_func ("/isas/pv/caracteristic_curve", test_isas_pv_caracteristic_curve);
	g_test_add_func ("/isas/radiation/eglobalhor_to_edirdiff_hor", test_isas_radiation_E_global_hor_TO_E_direct_hor_and_E_diffuse_hor);
	g_test_add_func ("/isas/radiation/edirhor_to_edirtilt", test_isas_radiation_E_direct_hor_TO_E_direct_tilt);
	return g_test_run ();
} 
