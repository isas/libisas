#!/usr/bin/python

__LICENSE__ = 'GPL-3'
__AUTHOR__ = 'Thomas Bechtold <thomasbechtold@jpberlin.de>'
__DESCRIPTION__ = 'Calculate the sunposition.'


import datetime
import sys

import argparse

import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

import gi
gi.require_version('Isas', '1.0')
from gi.repository import Isas


def argparse_options():
    parser = argparse.ArgumentParser(description=__DESCRIPTION__)
    parser.add_argument('latitude', type=float,
                        help=u'latitude in deg N')
    parser.add_argument('longitude', type=float,
                        help=u'longitude in deg E')
    parser.add_argument('year', type=int,
                        help='year')
    parser.add_argument('month', type=int,
                        help='month')
    parser.add_argument('day', type=int,
                        help='day')
    parser.add_argument('timezone', type=str,
                        help='timezone in format "+hh:mm"')
    parser.add_argument('--daily-stepsize', default=30, type=int,
                        help='the stepsize for a datetime interval in minutes. default is 30 minutes.')
    return vars(parser.parse_args())


if __name__ == '__main__':
    args = argparse_options()

    ax = plt.subplot(111)
    hours = mdates.HourLocator()
    hoursFmt = mdates.DateFormatter('%H')
    ax.xaxis.set_major_locator(hours)
    ax.xaxis.set_major_formatter(hoursFmt)

    dt_base = datetime.datetime(args['year'],args['month'],args['day'],0,0,0)
    dt_list = [ dt_base + datetime.timedelta(minutes=(x*args['daily_stepsize'])) for x in range(0,int(60.0/args['daily_stepsize']*24)) ]
    ax.set_title("Sunposition for %s-%s-%s (GMT %s) (Long: %s E / Lat: %s N)" % (dt_base.year, dt_base.month, dt_base.day, args['timezone'], args['longitude'], args['latitude']))    
    ax.set_xlabel('hour')
    ax.set_ylabel('Sun height in degrees')
    sun_height_list = list()
    for dt in dt_list:
        sun_height, sun_azimuth = Isas.sky_sunpos_DIN5034(args['longitude'], args['latitude'],dt.year , dt.month, dt.day, dt.hour, dt.minute, dt.second, args['timezone'])
        #print sun_height, sun_azimuth
        sun_height_list.append(sun_height)
    ax.plot(dt_list, sun_height_list, 'o-')
    ax.set_ylim(0, 90)
    plt.show()
