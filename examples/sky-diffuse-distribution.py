#!/usr/bin/python

__LICENSE__ = 'GPL-3'
__AUTHOR__ = 'Thomas Bechtold <thomasbechtold@jpberlin.de>'
__DESCRIPTION__ = '''
Calculate the diffuse sky radiation distribution with the Perez model.
Example: "python sky-diffuse-distribution 500 300 45 180"
'''


import datetime
import sys
import math

import argparse

import numpy as np
from pylab import *

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.figure import Figure

import gi
from gi.repository import Isas


def argparse_options():
    parser = argparse.ArgumentParser(description=__DESCRIPTION__)
    parser.add_argument('e-dir-hor', type=float,
                        help=u'direct horizontal radiation in W/(m*m)')
    parser.add_argument('e-diff-hor', type=float,
                        help=u'diffuse horizontal radiation in W/(m*m)')
    parser.add_argument('sun-height', type=float,
                        help=u'sun height in degrees')
    parser.add_argument('sun-azimuth', type=float,
                        help=u'sun azimuth in degrees')
    parser.add_argument('--solar-constant', type=float,
                        help=u'solar constant. default is 1367', default=1367)
    return vars(parser.parse_args())


if __name__ == '__main__':
    args = argparse_options()

    #args = {'solar_constant':1367, 'sun-height':45, 'sun-azimuth':180, 'e-dir-hor':500, 'e-diff-hor':300}

    #start calculation
    sky_brightness = Isas.sky_brightness(args['solar_constant'], args['sun-height'], args['e-diff-hor'])
    sky_clearness = Isas.sky_clearness(args['sun-height'], args['e-dir-hor'], args['e-diff-hor'])
    sky_zenith_luminance = Isas.sky_zenith_luminance(args['solar_constant'], args['sun-height'], args['e-dir-hor'], args['e-diff-hor'])

    res = np.zeros((90, 360))
    for (height, azimuth), val in np.ndenumerate(res):
        res[(height, azimuth)] = Isas.radiation_luminance_to_radiance(Isas.sky_point_luminance(args['sun-height'], args['sun-azimuth'], sky_zenith_luminance, height, azimuth, sky_brightness, sky_clearness)/360/90*math.pi)


    #plot
    fig = plt.figure()
    fig.set_size_inches(8,8)
    fig.patch.set_alpha(0.0)
    ax = fig.add_subplot(111)

    ax.set_title("diffuse radiation distribution (sum: %.2f W/(m*m))" % (sum(res)))
    ax.set_ylabel('height in degrees')
    ax.set_xlabel('azimuth in degrees')
    im = ax.imshow(res, cmap=cm.jet, aspect='auto')
    im.set_interpolation('bilinear')
    fig.colorbar(im)
    ax.grid(True)
    
    plt.show()
        

