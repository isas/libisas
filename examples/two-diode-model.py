#!/usr/bin/python

__LICENSE__ = 'GPL-3'
__AUTHOR__ = 'Thomas Bechtold <thomasbechtold@jpberlin.de>'
__DESCRIPTION__ = '''
Calculate the 2-diode model for a PV module.
Example: "python two-diode-model.py 10000 1 0.0000001 1 2 0.0257 3.17 0.00000000034 0.0000077 0.015 155"
'''


import datetime
import sys

import argparse

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

import gi
from gi.repository import Isas


def argparse_options():
    parser = argparse.ArgumentParser(description=__DESCRIPTION__)
    parser.add_argument('u-steps', type=int,
                        help=u'Number of steps to go from U=0V to U=U (open circuit)')
    parser.add_argument('u-start', type=float,
                        help=u'The start value for U (needed because numeric newton calculation)')
    parser.add_argument('u-abort-diff', type=float,
                        help='The difference value to stop the newton calculation. eg 0.00001')
    parser.add_argument('m1', type=float,
                        help='Diode factor')
    parser.add_argument('m2', type=float,
                        help='Diode factor')
    parser.add_argument('u-t', type=float,
                        help='temperature voltage (in Volt)')
    parser.add_argument('i-ph', type=float,
                        help='photo current (in Ampere)')
    parser.add_argument('i-s1', type=float,
                        help='saturation current (in Ampere)')
    parser.add_argument('i-s2', type=float,
                        help='saturation current (in Ampere)')
    parser.add_argument('r-s', type=float,
                        help='seriell resistor (in Ohm)')
    parser.add_argument('r-p', type=float,
                        help='parallel resistor (in Ohm)')
    return vars(parser.parse_args())


if __name__ == '__main__':
    args = argparse_options()

    #args = { 'u-steps':10000, 'u-start':1, 'u-abort-diff':0.000001, 'm1':1, 'm2':2, 'u-t':0.0257, 'i-ph':3.17, 'i-s1':0.00000000034, 'i-s1':0.0000077, 'r-s':0.015, 'r-p':155 }

    u_list, i_list, u_mpp, i_mpp = Isas.pv_module_characteristic_curve(args['u-steps'], args['u-start'], args['u-abort-diff'], 
                                                       args['m1'], args['m2'], args['u-t'], args['i-ph'], 
                                                       args['i-s1'], args['i-s1'], args['r-s'], args['r-p'])

    ax = plt.subplot(111)
    ax.set_title("characteristic curve pv module")
    ax.set_xlabel('U in Volt')
    ax.set_ylabel('I in Ampere')
    ax.annotate('MPP', xy=(u_mpp, i_mpp),  xycoords='data',
                xytext=(30, 30), textcoords='offset points',
                arrowprops=dict(arrowstyle="->")
                )
    ax.plot(u_list, i_list, '', u_mpp, i_mpp, 'ro', [u_mpp, u_mpp], [0, i_mpp], 'g', [0, u_mpp], [i_mpp, i_mpp], 'g' )

    ax.plot(u_list, i_list, '')
    ax.set_ylim(0, max(i_list)+0.5)


    plt.show()
        

